from django.contrib import admin
from singlemodeladmin import SingleModelAdmin
from apps.web.models import ( Categoria, Post, Funcionario, Alto_Funcionario)


@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    pass


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("titulo",)}
    

@admin.register(Funcionario)
class FuncionarioAdmin(admin.ModelAdmin):
    pass


@admin.register(Alto_Funcionario)
class Alto_FuncionarioAdmin(admin.ModelAdmin):
    pass


