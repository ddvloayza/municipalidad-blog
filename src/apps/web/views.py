import json
from django.shortcuts import render
from django.http.response import JsonResponse, HttpResponse
from pure_pagination import Paginator, PageNotAnInteger
from apps.web.models import (Categoria, Post, Funcionario, Alto_Funcionario)


def home(request):
    tipo_pagina = 'web'
    posts = Post.objects.all().order_by('-id')
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1
    p = Paginator(posts, 3, request=request)
    page_obj = p.page(page)
    print('page_obj', page_obj)

    return render(request, "index.html", locals())


def post_detalle(request, slug):
    post_detallado = Post.objects.get(slug=slug)
    ultimas_noticias = Post.objects.all().order_by('-id')[:3]

    return render(request, 'post_detalle.html', locals())

def altos_funcionarios(request):
    alto_funcionarios = Alto_Funcionario.objects.all()

    return render(request, 'altos_funcionarios.html', locals())

def funcionarios(request):
    funcionarios = Funcionario.objects.all()

    return render(request, 'funcionarios.html', locals())

def contacto(request):
    return render(request, 'contacto.html', locals())

def municipalidad(request):
    funcionarios = Funcionario.objects.all()

    return render(request, 'municipalidad.html', locals())

def mapa(request):
    funcionarios = Funcionario.objects.all()

    return render(request, 'mapa.html', locals())

def organigrama(request):

    return render(request, 'organigrama.html', locals())


def tahuamanu(request):

    return render(request, 'tahuamanu.html', locals())