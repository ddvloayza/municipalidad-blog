# Generated by Django 2.2.4 on 2020-01-31 14:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('web', '0004_auto_20191228_1142'),
    ]

    operations = [
        migrations.CreateModel(
            name='Funcionario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_funcionario', models.CharField(max_length=200, verbose_name='Nombre Funcionario')),
                ('cargo', models.CharField(max_length=200, verbose_name='Cargo')),
                ('imagen_principal', sorl.thumbnail.fields.ImageField(blank=True, help_text='Tamaño 664x362', max_length=200, null=True, upload_to='posts/', verbose_name='Imagen Principal')),
                ('usuario', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
