from django.urls import path
from django.conf.urls import url
from .views import (home, post_detalle, altos_funcionarios, funcionarios, municipalidad, 
					mapa, organigrama, tahuamanu, contacto)

app_name = 'web'

urlpatterns = [
	url(r'^$', home, name='home'),
	url(r'^post/(?P<slug>[-\w]+)/$', post_detalle, name='post_detalle'),
	url(r'^altos_funcionarios/$', altos_funcionarios, name='altos_funcionarios'),
	url(r'^funcionarios/$', funcionarios, name='funcionarios'),
	url(r'^municipalidad/$', municipalidad, name='municipalidad'),
	url(r'^mapa/$', mapa, name='mapa'),
	url(r'^organigrama/$', organigrama, name='organigrama'),
	url(r'^contacto/$', contacto, name='contacto'),
	url(r'^tahuamanu/$', tahuamanu, name='tahuamanu'),

]

