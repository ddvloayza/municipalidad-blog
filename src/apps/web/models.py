from django.db import models
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
from django.urls import reverse
from taggit.managers import TaggableManager
from ckeditor.fields import RichTextField


class Categoria(models.Model):
	usuario = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
	nombre = models.CharField('Nombre', max_length=200)
	active = models.BooleanField('Active', default=True)

	def __str__(self):
		return self.nombre


class Post(models.Model):
	categoria = models.ForeignKey(Categoria, related_name='categoria', on_delete=models.CASCADE, blank=True, null=True)
	numero_post = models.IntegerField('Numero', blank=True, null=True)
	fecha_emision = models.DateTimeField('Fecha de emision', blank=True, null=True)
	titulo = models.CharField('Titulo', max_length=5000, blank=True, null=True)
	resumen = models.TextField('Resumen', blank=True, null=True)
	descripcion = models.TextField(blank=True, null=True)
	imagen_principal = ImageField('Imagen Principal', max_length=200, upload_to='posts/', help_text="Tamaño 664x362", blank=True, null=True)
	video = models.URLField('Video url', help_text="La url obtenida por el link de Dropbox cambias el dl=0 x el dl=1", blank=True, null=True)
	slug = models.SlugField(max_length=2000, blank=True, null=True)
	tags = TaggableManager(blank=True, help_text='estos tags son para mejorar la busqueda')
	is_obra = models.BooleanField(default=False)


	class Meta:
		ordering = ['numero_post']

	def __str__(self):
		return self.titulo

	def get_absolute_url(self):
		return reverse('web:post_detalle', kwargs={'slug': self.slug})

class Funcionario(models.Model):
	usuario = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
	nombre_funcionario = models.CharField('Nombre Funcionario', max_length=200)
	cargo = models.CharField('Cargo', max_length=200)
	imagen_principal = ImageField('Imagen Principal', max_length=200, upload_to='posts/', help_text="Tamaño 664x362", blank=True, null=True)

	def __str__(self):
		return self.cargo


class Alto_Funcionario(models.Model):
	usuario = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
	nombre_alto_funcionario = models.CharField('Nombre Funcionario', max_length=200)
	cargo = models.CharField('Cargo', max_length=200)
	imagen_principal = ImageField('Imagen Principal', max_length=200, upload_to='posts/', help_text="Tamaño 664x362", blank=True, null=True)

	def __str__(self):
		return self.cargo
