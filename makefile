# donwload archivos
scp -i muni.pem ubuntu@3.217.46.89:backups/dump_18-06-2021_12_23_10.sql /Users/diegodelavega/municipalidad-blog/backups
#restore database
cat your_dump.sql | docker exec -i your-db-container psql -U postgres
#backup database
docker exec -t your-db-container pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql

docker cp NOMBRE_CONTENEDOR:RUTA_DEL_CONTENEDOR RUTA_LOCAL

docker cp 15f5aacaad4b:www/media backups

scp -i muni.pem ubuntu@3.217.46.89:backups/media.zip /Users/diegodelavega/municipalidad-blog/backups